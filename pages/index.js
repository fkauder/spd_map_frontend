import React from 'react';
import dynamic from 'next/dynamic'
import Head from 'next/head';
import {useWindowSize} from '../utils/hooks';

export default function Home() {
    const InteractiveMap = typeof(window) !== 'undefined' && dynamic(
        () => import('../modules/geo/components/InteractiveMap')
    );
    const windowSize = useWindowSize();
    return (
        <div>
            <Head>
                <title>Create Next App</title>
                <meta name="description" content="Generated by create next app"/>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
            {InteractiveMap && <InteractiveMap windowSize={windowSize} />}
        </div>
    )
}
