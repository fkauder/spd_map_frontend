import React from 'react';
import styled from 'styled-components';
import {MapContainer, Marker, Popup, TileLayer} from "react-leaflet";
import 'leaflet/dist/leaflet.css';
import DistrictLayer from './DistrictLayer';
import ContributionLayer from './ContributionLayer';

let InteractiveMap = ({className}) => {
    const position = [52.501757285900624, 13.445608308014762];
    return (
        <MapContainer
            center={position}
            className={className}
            zoom={13}
            scrollWheelZoom={true}>
            <DistrictLayer />
            <ContributionLayer />
            <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
        </MapContainer>
    );
};
InteractiveMap = styled(InteractiveMap)`
    background-color: #eee;
    height: ${props => props.windowSize.height}px;
    width: 100%;
`;

export default InteractiveMap;
