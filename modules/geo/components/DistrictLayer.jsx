import React, {useState} from 'react';
import _ from 'lodash';
import {GeoJSON} from 'react-leaflet';
import {useQuery} from 'react-query';
import chroma from 'chroma-js';

const DistrictLayer = () => {
    const {isLoading, error, data} = useQuery('agh_districts', () =>
        fetch('/api/v1/elections/agh_districts/')
            .then(res => res.json())
            .catch(err => console.error(err))
    );
    const geoJson = _.map(data, item => {
        const feature = _.cloneDeep(item.borders);
        feature.properties = _.reduce(
            item,
            (collection, value, key) => {
                if (key !== 'borders') {
                    collection[key] = value;
                }
                return collection;
            },
            {}
        );
        return feature;
    });
    if (!geoJson) {
        return null;
    }

    const applyStyle = feature => {
        return {
            color: '#000',
            fillColor: chroma.temperature(feature.geometry.properties.number * 750),
            fillOpacity: 0.25
        };
    };

    return (
        <GeoJSON
            key={geoJson.length}
            data={geoJson}
            interactive={false}
            style={applyStyle} />
    );
};

export default DistrictLayer;
