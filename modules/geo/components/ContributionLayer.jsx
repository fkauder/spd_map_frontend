import React, {useState} from 'react';
import _ from 'lodash';
import {GeoJSON} from 'react-leaflet';
import {useQuery} from 'react-query';

const ContributionLayer = () => {
    const {isLoading, error, data} = useQuery('contributions', () =>
        fetch('/api/v1/contributions/')
            .then(res => res.json())
            .catch(err => console.error(err))
    );
    const geoJson = _.map(data, item => {
        const feature = _.cloneDeep(item.geometry);
        feature.properties = _.reduce(
            item,
            (collection, value, key) => {
                if (key !== 'geometry') {
                    collection[key] = value;
                }
                return collection;
            },
            {}
        );
        return feature;
    });
    if (!geoJson) {
        return null;
    }

    const applyStyle = feature => {
        return {
            color: '#e2001a',
            fillColor: '#e2001a',
            fillOpacity: 0.5
        };
    };

    const onEachFeature = (feature, layer) => {
        layer.on({
            click: () => console.log(feature),
            mouseover: () => console.log(feature)
        });
    };

    return (
        <GeoJSON
            key={geoJson.length}
            data={geoJson}
            onEachFeature={onEachFeature}
            style={applyStyle} />
    );
};

export default ContributionLayer;
